from dash.dependencies import Input, Output
import dash_table

PAGE_SIZE = 20


def dashboard(df, app):
    df, train_acc, test_acc = df
    app.layout = dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict("rows"),

        style_data_conditional = [{
            'if': {'column_id': 'prob'},
            'backgroundColor': '#3D9970',
            'color': 'white',
        }],

        pagination_settings = {'current_page': 0,
                               'page_size': PAGE_SIZE},

        style_table = {'overflowX': 'scroll'})


    return app


def dashboard_ext(df, app):
    df, train_acc, test_acc = df
    print(f'Train: {train_acc}, Test: {test_acc}, \nDF: {df.head()}')
    app.layout = dash_table.DataTable(
        id='table-sorting-filtering',
        columns=[
            {'name': i, 'id': i, 'deletable': True} for i in sorted(df.columns)
        ],

        style_data_conditional=[{
            'if': {'column_id': 'prob'},
            'backgroundColor': '#3D9970',
            'color': 'white',
        }],

        pagination_settings={
            'current_page': 0,
            'page_size': PAGE_SIZE
        },

        style_table={'overflowX': 'scroll'},

        pagination_mode='be',

        filtering='be',
        filtering_settings='',

        sorting='be',
        sorting_type='multi',
        sorting_settings=[]
    )

    @app.callback(
        Output('table-sorting-filtering', 'data'),
        [Input('table-sorting-filtering', 'pagination_settings'),
         Input('table-sorting-filtering', 'sorting_settings'),
         Input('table-sorting-filtering', 'filtering_settings')])
    def update_graph(pagination_settings, sorting_settings, filtering_settings):
        filtering_expressions = filtering_settings.split(' && ')
        dff = df
        for filter in filtering_expressions:
            if ' eq ' in filter:
                col_name = filter.split(' eq ')[0]
                filter_value = filter.split(' eq ')[1]
                dff = dff.loc[dff[col_name] == filter_value]
            if ' > ' in filter:
                col_name = filter.split(' > ')[0]
                filter_value = float(filter.split(' > ')[1])
                dff = dff.loc[dff[col_name] > filter_value]
            if ' < ' in filter:
                col_name = filter.split(' < ')[0]
                filter_value = float(filter.split(' < ')[1])
                dff = dff.loc[dff[col_name] < filter_value]

        if len(sorting_settings):
            dff = dff.sort_values(
                [col['column_id'] for col in sorting_settings],
                ascending=[
                    col['direction'] == 'asc'
                    for col in sorting_settings
                ],
                inplace=False
            )

        return dff.iloc[
            pagination_settings['current_page']*pagination_settings['page_size']:
            (pagination_settings['current_page'] + 1)*pagination_settings['page_size']
        ].to_dict('rows')
    return app
