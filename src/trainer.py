import xgboost as xgb
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
from xgboost import XGBClassifier


def train_xg(csv):
    data = pd.read_csv(csv)
    rand_split = np.random.rand(len(data))
    data['split_col'] = rand_split
    data = data.sort_values(by=['split_col'])

    # train_test ratio adjustment
    train_df, test_df = train_test_split(data)

    train_data = train_df.values
    test_data = test_df.values

    dtrain = xgb.DMatrix(train_data[:, [0, 23]], label=train_data[:, 24])
    dtest = xgb.DMatrix(test_data[:, [0, 23]], label=test_data[:, 24])

    param = {'max_depth': 6, 'eta': 0.1, 'silent': 1, 'objective': 'binary:logistic', 'gamma': 1}
    param['eval_metric'] = 'auc'
    # max depth
    # eta
    # gamma

    model_cv = xgb.cv(param, dtrain, num_boost_round=100, nfold=5, show_stdv=True, early_stopping_rounds=50,
                      verbose_eval=False)
    # nfold
    # round (epoch)

    evallist = [(dtrain, 'train')]
    model = xgb.train(param, dtrain, num_boost_round=100, evals=evallist,
                      verbose_eval=False)

    train_pred_prob = model.predict(dtrain)
    test_pred_prob = model.predict(dtest)

    train_pred = train_pred_prob > 0.5
    test_pred = test_pred_prob > 0.5
    # prob rounding > 1
    # treshold value

    train_acc = np.mean(train_pred.astype(int) != train_data[:, 24] - 1)
    test_acc = np.mean(test_pred.astype(int) != test_data[:, 24] - 1)

    df = pd.concat([train_df, test_df])
    df['prob'] = np.concatenate([train_pred_prob, test_pred_prob])
    df = df.reset_index(drop=True)
    df = df.drop(columns=['split_col'])

    # print(f'DF: {df.head()} \n'
    #       f'Train error: {train_acc} \n'
    #       f'Test error:  {test_acc}')

    return df, train_acc, test_acc
