import dash_table
from flask import Flask, render_template, request, url_for, redirect
import dash
import dash_html_components as html

from src.trainer import train_xg
from src.dashboard import dashboard

server = Flask(__name__)


@server.route('/')
def index():
    return render_template("index.html")


@server.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save(f.filename)
        df = train_xg(f.filename)
        dashboard(df, app)
        return redirect(url_for('/learn/'))


app = dash.Dash(
    __name__,
    server=server,
    routes_pathname_prefix='/learn/'
)

app.layout = html.Div("XGBoost Machine Learning")


def make_layout(df):
    app.layout = dash_table.DataTable(
        id='table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict("rows"),
    )


if __name__ == '__main__':
    app.run_server(debug=True)
